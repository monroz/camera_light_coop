from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

# fetch values from package.xml
setup_args = generate_distutils_setup(
    packages=['camera_light_coop'],
    scripts=['scripts'],
    package_dir={'': 'src'},
)

setup(**setup_args)
