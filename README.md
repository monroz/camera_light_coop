# Cooperative exploration of dark spaces using heterogenous robots
Final project for CS169 Multirobot Systems.

Simulation of cooperative robot exploration of dark space. A Turtlebot with a 
light traverses a region following a lawnmower pattern and stops when it reaches any
objects in the space. When it reaches an object, the light comes on and a service call is 
sent to the camera robot. The camera robot moves to the object and takes a picture of the 
object. The light robot receives a service call when the camera robot is finished and 
continues on the lawnmower pattern.

## Getting Started
To run the code, clone the repository into a catkin workspace. 

```
class@ubuntu:~$ git clone https://gitlab.com/monroz/camera_light_coop.git
```

Once the repository has been cloned, use the catkin_make command and source the workspace.

```
class@ubuntu:~/catkin_ws$ catkin_make
class@ubuntu:~/catkin_ws$ source devel/setup.bash
```

To launch the simulation, execute the following command:

```
class@ubuntu:~/catkin_ws$ roslaunch camera_light_coop coop_sim.launch

```

## Built With

* ROS Kinetic - ROS version used
* Gazebo Simulator - Simulator used

## Authors
**Author: Monika Roznere<br />
Maintainer: Monika Roznere, monika.roznere.gr@dartmouth.edu**

**Author: Amy Sniffen<br />
Maintainer: Amy Sniffen, amy.k.sniffen.gr@dartmouth.edu**

