from shapely.geometry import Polygon, LineString
from math import pi, hypot
import tf

def min_and_max_y(region, x):
    """
    Finds the minimum and maximum y values of a region

    Args:
        region: polygon

        x: max x value in region
    """

    _, min_y, _, max_y = region.bounds
    x_line = LineString([(x, min_y), (x, max_y)])
    return x_line.intersection(region).xy[1].tolist()


def distance(point_1, point_2):
    """
    Function to get distance between two points

    Args:
        point_1: first tuple
        point_2: second tuple

    Returns hypotenuse between them
    """

    x1, y1 = point_1
    x2, y2 = point_2

    return hypot(x2 - x1, y2 - y1)


def rectify_angle(angle):
    """
    Adjusts the angle to be in the range [-pi, pi]

    Args:
        angle: Angle that needs to be adjusted.
    Returns:
        The angle adjusted in the range [-pi, pi]
    """

    # adjust angle to be in the range [0, 2pi]
    while(angle) < 0:
        angle += 2*pi
    while(angle) > 2*pi:
        angle -= 2*pi

    # angles over pi should wrap around in the range [-pi, 0]
    if (angle > pi):
        angle -= 2*pi

    return angle


def quat_to_euler(odom):
    """
    Convery quaternion to euler angles.

    Args:
        odom: Orientation parameterized using quaternions.
    Returns:
        Orientation represented as yaw.
    """
    quaternion = (
        odom.x,
        odom.y,
        odom.z,
        odom.w
    )
    roll, pitch, yaw = tf.transformations.euler_from_quaternion(quaternion)

    return yaw
