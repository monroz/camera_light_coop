import rospy
import numpy
import math

# message types for amcl_pose and base_scan topics, respectively
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import Image
from std_srvs.srv import Trigger, TriggerResponse, TriggerRequest

from cv_bridge import CvBridge, CvBridgeError

from camera_light_coop.math_helper import *
from camera_light_coop.image_process import *

LOCATE_LIGHT = "locate light"
FOLLOW_LIGHT = "follow light"
WAIT_LIGHT = "wait light"
STAY = "stay"
SCREENSHOT = "screenshot"

class CameraRobot:
    """
    Class used to create and traverse lawnmower pattern for the light robot

    Methods
    -------
    - pose_callback
    - scan_callback
    - image_callback
    - handle_start_snapshot
    - trigger_light_robot
    - spin

    """

    def __init__(self):
        """
        Attributes
        ----------

        state: the current state that the robot is in, which corresponds to the
            actions that it will take
        SAFE_DISTANCE: the minimum distance the robot may be in relation to an
            obstacle
        LINEAR_SPEED: the linear speed of the robot
        ANGULAR_SPEED: the angular speed of the robot
        cur_pose: the current local x and y coordinates of the robot (NOT USED)
        cur_orient: the current yaw of the robot (NOT USED)
        """

        self.state = STAY

        # parameters for obstacle avoidance and velocity
        self.SAFE_DISTANCE = 0.5
        self.LINEAR_SPEED = 0.1
        self.ANGULAR_SPEED = 0.08

        # cur pose and orientation of the robot
        # initialized to 0 as a place holder
        self.cur_pose = (0.0, 0.0)
        self.cur_orient = 0.0

        self.bridge = CvBridge()

        self.cmd_pub = rospy.Publisher("/camera_robot/mobile_base/commands/velocity", Twist, queue_size=1)

        rospy.Subscriber("/camera_robot/odom", Odometry, self.pose_callback) # current robot's position and orientation
        rospy.Subscriber("/camera_robot/scan", LaserScan, self.scan_callback) # current robot's laser scan readings
        rospy.Subscriber("/camera_robot/camera/rgb/image_raw", Image, self.image_callback) # process current image seen by robot

        self.start_snapshot = rospy.Service("/camera_robot/start_snapshot", Trigger, self.handle_start_snapshot)
        self.light_robot_service = rospy.ServiceProxy('/light_robot/start_exploration', Trigger)


    def pose_callback(self, msg):
        """
        Callback for retrieving the robot's position and orientation
        This is not used currently, but will be used in the future extensions

        Args:
            msg: Odometry msg that has the robot's position and orientation.
        """

        self.cur_pose = (msg.pose.pose.position.x, msg.pose.pose.position.y)
        self.cur_orient = quat_to_euler(msg.pose.pose.orientation)


    def scan_callback(self, msg):
        """
        Callback for retrieving the robot's laser scan readings. This funciton
        makes sure that no obstacles are within the robot's safety distance. If
        so it will change the robot's state for obstacle avoidance.

        Args:
            msg: LaserScan msg describing the robot's laser scan readings.
        """

        ranges = numpy.array(msg.ranges)
        angle_min = msg.angle_min
        angle_inc = msg.angle_increment

        # start off with the largest possible scan reading
        min_laser_scan_reading = msg.range_max

        # find the smallest laser scan reading in front of the robot
        for i in range(len(ranges)):
            if -math.pi / 4 < angle_min + angle_inc * i < math.pi / 4:
                if ranges[i] < min_laser_scan_reading:
                    min_laser_scan_reading = ranges[i]
                    # print("angle: " + str(angle_min + angle_inc * i) + " range: " + str(min_laser_scan_reading))

        # there is an obstacle in the robot's safety area
        if min_laser_scan_reading < self.SAFE_DISTANCE:
            print("Obstacle near by, stopping.")
            self.state = STAY


    def image_callback(self, msg):
        """
        Callback for retrieving the robot's current camera frame. This function
        updates the state of the robot.

        Args:
            msg: Image msg that contains the image frame taken by the robot's camera.
        """

        self.image = self.bridge.imgmsg_to_cv2(msg, "bgr8")

        if self.state == LOCATE_LIGHT:
            light = find_brightness(self.image)

            if light:
                self.state = FOLLOW_LIGHT

        elif self.state == FOLLOW_LIGHT:
            good_view = track_brightness(self.image)

            if good_view:
                self.state = STAY
                self.trigger_light_robot()
            else:
                self.state = FOLLOW_LIGHT


    def handle_start_snapshot(self, req):
        """
        Callback for a request to stop the camera robot to locate light.

        Args:
            msg: Trigger msg.
        Rerurn:
            boolean and message acknowledging the completion of the request
        """

        self.state = LOCATE_LIGHT
        print("Camera robot taking footage of illuminated object.")

        return TriggerResponse(True, "Camera robot locating light.")


    def trigger_light_robot(self):
        """
        Function for signaling the light robot to start exploring the environment.
        """

        rospy.wait_for_service('/light_robot/start_exploration')
        try:
            trig = TriggerRequest()
            result = self.light_robot_service(trig)
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e


    def spin(self):
        """
        The robot should continously act in some behavior.
        """

        r = rospy.Rate(10)

        cmd_msg = Twist()
        while not rospy.is_shutdown():

            if self.state == LOCATE_LIGHT:

                cmd_msg.linear.x = 0
                cmd_msg.angular.z = self.ANGULAR_SPEED

            elif self.state == FOLLOW_LIGHT:
                cmd_msg.linear.x = self.LINEAR_SPEED
                cmd_msg.angular.z = 0

            else:
                cmd_msg.linear.x = 0
                cmd_msg.angular.z = 0

            self.cmd_pub.publish(cmd_msg)

            r.sleep()
