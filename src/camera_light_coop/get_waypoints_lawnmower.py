from shapely.geometry import Polygon
import numpy as np
from camera_light_coop.math_helper import min_and_max_y


def generate_waypoints(region, lane_count = 10):
    """
    Function to generate waypoints inside a given region

    Args:
        region: polygon

        lane_count: number of lanes for lawnmower pattern. Default is 10

    Returns list of waypoints evenly spaced in region
    """

    #get bounds of region
    (min_x, _, max_x, _) = region.bounds

    x_coords = np.linspace(min_x, max_x, num=lane_count)

    #which direction traversing the region
    going_up = True
    waypoints = []

    for x_coord in x_coords:

        #find intersections
        y_intersections = min_and_max_y(region, x_coord)

        if len(y_intersections) == 1:
            points = [(x_coord, y_intersections[0])]

        else:

            y_min = y_intersections[0]
            y_max = y_intersections[1]

            points = [(x_coord, y_min), (x_coord, y_max)]

            #reverse points if not going up
            if not going_up:

                points.reverse()

        waypoints += points
        going_up = not going_up

    return waypoints
