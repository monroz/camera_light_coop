import rospy
from  shapely.geometry import Polygon
import numpy as np
import tf

from camera_light_coop.math_helper import *
from camera_light_coop.get_waypoints_lawnmower import generate_waypoints

from geometry_msgs.msg import Twist
from geometry_msgs.msg import Point
from nav_msgs.msg import Odometry
from sensor_msgs.msg import LaserScan
from std_srvs.srv import Trigger, TriggerResponse, TriggerRequest

from math import atan2


class LightRobot:

    """
    Class used to create and traverse lawnmower pattern for the light robot

    Methods
    -------
    - get_region
    - base_pose_callback
    - determine_waypoint
    - handle_start_exploration
    - trigger_camera_robot
    - move
    - laser_scan_callback
    - spin

    """

    def __init__(self):
        """
        Attributes
        ----------

        region: the specified region the light robot will be traversing

        waypoints: the points in the region the light robot will be traveling to

            *Note: region and waypoints were not used in the simulation show in the final presentation

        target: hard-coded waypoints for robot to travel to

        """

        self.region = self.get_region()
        self.waypoints = generate_waypoints(self.region)

        #waypoints along lawnmower pattern
        self.target = [(-8.0,8.0), (-8.0,-8.0), (-6.0,-8.0), (-6.0,8.0), (-4.0, 8.0), (-4.0,-8.0), (-2.0,-8.0), (-2.0,8.0), (0.0,0.0), (2.0, 8.0), (4.0, 8.0), (4.0,-8.0), (6.0, -8.0), (6.0,8.0), (8.0,8.0), (8.0,-8.0)]
        self.count = 0
        #set to false when traveling to waypoint
        #true when arrived at waypoint
        self.target_reached = False
        self.is_moving = True

        rospy.Subscriber("/light_robot/odom", Odometry, self.base_pose_callback)
        #rospy.Subscriber("/light_robot/base_scan", LaserScan, self.laser_scan_callback)

        self.cmd_msg = rospy.Publisher("/light_robot/mobile_base/commands/velocity", Twist, queue_size = 1)

        self.start_exploration = rospy.Service("/light_robot/start_exploration", Trigger, self.handle_start_exploration)
        self.camera_robot_service = rospy.ServiceProxy('/camera_robot/start_snapshot', Trigger)


    def get_region(self):
        """
        takes region and returns a polygon of specified region

        Args:
            region: tuples in region
        """

        #chosen based on dimensions of region
        region = [(1,1),(9,1),(9,9),(1,9)]

        #returns polygon from shapely library
        return Polygon(region)


    def base_pose_callback(self, pose_msg):
        """
        Callback to get current orientation and location of robot

        Args:
            pose_msg: odom message
        """

        quaternion = (
            pose_msg.pose.pose.orientation.x,
            pose_msg.pose.pose.orientation.y,
            pose_msg.pose.pose.orientation.z,
            pose_msg.pose.pose.orientation.w)

        euler = tf.transformations.euler_from_quaternion(quaternion)

        # current (x,y,theta) of robot
        self.current_state = (pose_msg.pose.pose.position.x, pose_msg.pose.pose.position.y, euler[2])

        #choose next waypoint to go to
        self.determine_waypoint()


    def determine_waypoint(self):
        """
        Function to get next waypoint in target list.
        Determines next waypoint based on target_reached flag
        """

        #if target is set but we're not there yet
        if self.target_reached == False:
            #continue with same waypoint
            goal = self.target[self.count]
            self.move(goal)

        #we've reached it and we need a new goal
        if self.target_reached == True:
            if self.count < len(self.target):
                #continue to new waypoint - count has been increased by one
                goal = self.target[self.count]
                self.move(goal)


    def handle_start_exploration(self, req):
        """
        Callback for a request to start the light robot to explore world.

        Args:
            msg: Trigger msg.
        Rerurn:
            boolean and message acknowledging the completion of the request
        """

        self.is_moving = True
        print("Light robot exploring world.")
        return TriggerResponse(True, "Light robot exploring world.")


    def trigger_camera_robot(self):
        """
        Function for signaling the camera robot to start taking a snapshot of
        the illuminated object.
        """

        rospy.wait_for_service('/camera_robot/start_snapshot')
        try:
            trig = TriggerRequest()
            result = self.camera_robot_service(trig)
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e


    def move(self, goal):
        """
        Function to find next move for robot

        Args:
            goal: tuple containing desired (x,y) coordinates
        """

        if(self.is_moving):

            move = Twist()

            move.linear.x = 0.0
            move.angular.z = 0.0

            self.cmd_msg.publish(move)

            #checks if the difference between current x and goal x/current y and goal y is greater than 0.5
            if (abs(self.current_state[0] - goal[0]) > 0.5) or (abs(self.current_state[1] - goal[1]) > 0.5):

                #if difference of either is larger, waypoint hasn't been reached
                self.target_reached = False

                #determine desired heading based on goal location and current location
                target_angle = atan2(goal[1] - self.current_state[1], goal[0] - self.current_state[0])

                #rectify is a helper function location in helpers.py
                target_angle = rectify_angle(target_angle)

                #if difference between target orientation and current orientation is too large, adjust
                if (abs(target_angle - self.current_state[2])) > 0.3:
                    move.linear.x = 0.1
                    move.angular.z = 0.3

                else:
                    #else continue on
                    move.linear.x = 0.3
                    
                self.cmd_msg.publish(move)

            #if both differences are less, then robot has arrived
            elif (abs(self.current_state[0] - goal[0]) < 0.5) and (abs(self.current_state[1] - goal[1]) < 0.5):
                self.target_reached = True
                self.count += 1

                #if robot arrived at shelf, send trigger to camera robot
                if goal[0] == 0.0 and goal[1] == 0.0:
                    self.is_moving = False
                    self.trigger_camera_robot()

                if self.count == len(self.target):
                    print("Search Complete")



    def laser_scan_callback(self, laser_msg):
        """
        Callback for laser scan topic

        Args:

            laser_msg: message containing laser scan data
        """

        self.ranges = numpy.array(laser_msg.ranges)

        self.step_size = laser_msg.angle_increment
        self.angle_min = laser_msg.angle_min


    def spin(self):
        """
        The robot should continously act in some behavior.
        """

        r = rospy.Rate(2)

        while not rospy.is_shutdown():
            rospy.spin()
