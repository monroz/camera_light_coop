import cv2

def find_brightness(image):
    """
    Calculates the amount of bright pixels in the scene.

    Args:
        image: current frame from the robot's camera.
    Returns:
        light: boolean, assumption if there is a light source seen in the frame.
    """

    light = False

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    height, width = gray.shape[:2]

    for i in range(height):
        for j in range(width):
            if 127 <= gray[i, j]:
                light = True
                break

    return light


def track_brightness(image):
    """
    Calculates if the region of brightness in the image frame fits optimally in
    the current frame enclosure.

    Args:
        image: current frame from the robot's camera.
    Returns:
        good_view: boolean, assumption if the bright region is optimally in the
            frame enclosure or not
    """

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
    height, width = thresh.shape[:2]

    good_view = False

    # checks the top and bottom frame
    for j in range(width):
        if thresh[20, j] >= 127 or thresh[height-20, j] >= 127:
            good_view = True
            break

    return good_view
