#!/usr/bin/python

import rospy

from camera_light_coop.camera_bot import CameraRobot

if __name__ == "__main__":
    """
    Initialize camera robot
    """
    rospy.init_node("camera_robot_live", anonymous=False)

    robot = CameraRobot()
    robot.spin()
