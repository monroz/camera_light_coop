#!/usr/bin/python

import rospy

from camera_light_coop.light_bot import LightRobot

if __name__ == "__main__":
    """
    Initialize light robot
    """
    rospy.init_node("light_robot_live", anonymous=False)

    robot = LightRobot()
    robot.spin()
